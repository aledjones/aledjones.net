const getTweet = require('./modules/twitter');

const fs = require('fs');
const path = require('path');

const yes = require('yes-https');

const express = require('express')
const app = express();

const diskdb = require('diskdb');
const db = diskdb.connect('./data', ['portfolio', 'blog', 'assets']);

// app.use(yes());

// latest tweet 
app.get('/api/tweet', function (req, res) {
    const username = req.query.username;
    if (!username) {
        res.status(500).send('MALFORMED_REQUEST');
    } else {
        getTweet(username)
            .then(function (tweet) {
                res.json(tweet);
            }, function (err) {
                res.status(500).send('UNKNOWN_ERROR');
            });
    }
});

// complete portfolio
app.get('/api/portfolio', function (req, res) {
    const posts = db.portfolio.find() || [];
    res.json(posts);
});

// get blog posts
app.get('/api/blog', function (req, res) {
    const posts = db.blog.find() || [];
    res.json(posts);
});

// get full blog post
app.get('/api/blog/post', function (req, res) {
    const id = req.query.id;
    if (!id) {
        res.status(500).send('MALFORMED_REQUEST');
    } else {
        const post = db.blog.findOne({ _id: id });
        fs.readFile('./html/' + post.html, 'utf8', function (err, html) {
            if (err) {
                res.status(500).send('PARSE_ERROR');
            } else {
                post.html = html;
                res.json(post);
            }
        });
    }
});

app.get('/api/asset', function (req, res) {
    const id = req.query.id;
    if (!id) {
        res.status(500).send('MALFORMED_REQUEST');
    } else {
        const asset = db.assets.findOne({ _id: id });
        if (!asset) {
            res.status(404).send('NOT_FOUND');
        } else {
            res.sendFile(path.join(__dirname, './assets/', asset.filename));
        }
    }
});

app.use(express.static(path.join(__dirname, 'dist')));

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

const server = app.listen(process.env.PORT || 3000, () => {
    console.log('App listening on port %s', server.address().port);
});