<h3>A brick wall what now?</h3>
<p>One of my current projects required the creation of a masonry style grid layout. This layout, shown below, is a nice way
    of displaying items in a space efficient and interesting way.</p>
<figure>
    <img src="/api/asset?id=2" alt="Masorary grid layout example">
    <figcaption>Fig 1. A demonstration of the desired outcome</figcaption>
</figure>

<h3>Specification</h3>
<p>A vertically stacked grid of 'tiles' with a fixed padding between each item. In addition to this basic outline:</p>
<ul>
    <li>Items can be of any height, this should never have to be explicitly defined.</li>
    <li>Items should 'back-fill' any space left by shorter content.</li>
    <li>The padding and column count should be easily configurable.</li>
    <li>The first item should optionally span the first two columns.</li>
    <li>The solution should be easily reusable in many contexts.</li>
</ul>

<h3>Solution</h3>
<p>After initially explored the possibility of using a fully CSS based approach. It quickly became evident that, even though
    the layout could be achieved, it required a lot of content specific styling and so was not easily reusable. I wanted
    an solution that was more agnostic in its approach. Javascript seemed the natural place to look and as the project was
    using Angular already, it made sense for this to be an Angular directive.</p>
<p>To produce the desired effect the layout needed to be broken down into two parts. Firstly a ‘container directive’ to deal
    with coordinating the position of each of the grid items. Secondly a much simpler ‘item directive’ whose main responsibility
    would be to bind the calculated positions to each item.</p>

<code type="grid-container.directive.ts">
<span>import { Directive, Input, Renderer2, OnInit, AfterContentInit, OnDestroy, ElementRef, HostListener, HostBinding, ContentChildren, QueryList } from '@angular/core';</span>
<span>import { GridItemDirective } from './grid-item.directive';</span>
<span></span>
<span>@Directive({</span>
<span>    selector: '[appGridContainer]'</span>
<span>})</span>
<span>export class GridContainerDirective implements OnInit, AfterContentInit, OnDestroy {</span>
<span></span>
<span>    public height: number;</span>
<span>    public itemsSub: any;</span>
<span></span>
<span>    @Input() gridCols = 3;</span>
<span>    @Input() gridPadding = 20;</span>
<span>    @Input() gridFeature = true;</span>
<span></span>
<span>    @ContentChildren(GridItemDirective) items: QueryList<GridItemDirective>;</span>
<span></span>
<span>    @HostListener('window:resize') resize() {</span>
<span>        this.reflow();</span>
<span>    }</span>
<span></span>
<span>    @HostBinding('style.height.px') get bindHeight() {</span>
<span>        return this.height;</span>
<span>    }</span>
<span></span>
<span>    constructor(</span>
<span>        private renderer: Renderer2,</span>
<span>        private element: ElementRef</span>
<span>    ) { }</span>
<span></span>
<span>    public ngOnInit() {</span>
<span>        const position = this.element.nativeElement.style.position;</span>
<span>        if (position === '' || position === 'static') {</span>
<span>            this.renderer.setStyle(this.element.nativeElement, 'position', 'relative');</span>
<span>        }</span>
<span>    }</span>
<span></span>
<span>    public ngAfterContentInit() {</span>
<span>        this.reflow();</span>
<span>        this.itemsSub = this.items.changes.subscribe(change => {</span>
<span>            this.reflow();</span>
<span>        });</span>
<span>    }</span>
<span></span>
<span>    public ngOnchanges() {</span>
<span>        this.reflow();</span>
<span>    }</span>
<span></span>
<span>    private indexOfLowestValue(arr) {</span>
<span>        const min = Math.min(...arr);</span>
<span>        return arr.indexOf(min);</span>
<span>    }</span>
<span></span>
<span>    public reflow() {</span>
<span>        const columns = new Array(this.gridCols).fill(0);</span>
<span></span>
<span>        this.items.forEach((item, i) => {</span>
<span></span>
<span>            const colSpan = (this.gridCols > 1 && this.gridFeature && i === 0) ? 2 : 1;</span>
<span>            const width = 'calc(((100% / ' + this.gridCols + ') * ' + colSpan + ') - (' + (this.gridPadding * (this.gridCols - colSpan)) + 'px / ' + this.gridCols + '))';</span>
<span>            this.renderer.setStyle(item.element.nativeElement, 'width', width);</span>
<span></span>
<span>            const row_i = this.indexOfLowestValue(columns);</span>
<span>            const box = item.element.nativeElement.getBoundingClientRect();</span>
<span>            item.left = (box.width * row_i) + (row_i * this.gridPadding);</span>
<span>            item.top = columns[row_i];</span>
<span></span>
<span>            columns[row_i] = columns[row_i] + box.height + this.gridPadding;</span>
<span>            if (this.gridCols > 1 && this.gridFeature && i === 0) {</span>
<span>                columns[row_i + 1] = columns[row_i + 1] + box.height + this.gridPadding;</span>
<span>            }</span>
<span></span>
<span>        });</span>
<span></span>
<span>        this.height = Math.max(...columns) - this.gridPadding;</span>
<span>    }</span>
<span></span>
<span>    ngOnDestroy() {</span>
<span>        this.itemsSub.unsubscribe();</span>
<span>    }</span>
<span></span>
<span>}</span>
</code>

<p>The grid container is where the majority of the heavy lifting is done. The parent directive makes substantial use of Angular’s
    <a target="_blank" href="https://angular.io/guide/lifecycle-hooks">lifecycle hooks</a>. These are standardised function calls that are triggered on the directive’s class at important points
    in a directives lifecycle - from initial creation right through to destruction.</p>

<p>During the ‘OnInit’ hook, triggered once the inputs are initially checked and set, the container directive applies a position
    styling of 'relative' to the container element. The grid items will be positioned absolutely but this only works within
    a parent whose position value is none 'static' (the default positioning mode).</p>

<p>The reflow function is the main guts of the directive. It's job is to calculate the required position of each item to form
    the grid pattern. The function initialises with an array, 'columns', that will contain a cumulative height for each of
    the columns as items are added. The grid items are looped and for each, the following takes place:</p>

<ol>
    <li>We calculate the width of each item based on a percentage of the parent.</li>
    <li>The index of the current shortest column is found.</li>
    <li>The grid item's position is calculated based on the selected column.</li>
    <li>The space taken up by this new element is added to the total height of the selected column.</li>
</ol>

<p>This is repeated until all grid items have been positioned.</p>

<p>Absolutely positioned elements do not affect the height of their parent element so, using the columns array which now contains
    the full height of each of the filled columns, the greatest value is assigned to the container's height.</p>

<p>Reflowing of the grid can be triggered by 3 events:</p>

<ol>
    <li>Items are added or removed from the grid.</li>
    <li>The configuration variables change.</li>
    <li>The window is resized.</li>
</ol>

<p>To account for items being added or removed we use the 'ContentChildren' decorator to tell Angular to generate a dynamically
    updating list of elements using the ‘GridItemDirective’ as a selector. ‘AfterContentInit’ is another of Angular's lifecycle
    hooks that is triggered after the element's content has been initialised. It is at this point that we can safely set
    up a subscription to the changes observable on the items property. Every time a grid item is created or destroyed, we
    will be informed via this subscription. It is at this point we want to reflow the grid.</p>

<p>The ‘OnChanges’ lifecycle hook is triggered every time an input is changed allowing the reflow function to be called whenever
    the input configuration changes.</p>

<p>Using a ‘HostListener’ decorator we can attach an event listener to the window that triggers when the window is resized,
    calling the reflow function once more. Note that in this case we pass a false value into the reflow function. This misses
    out the expensive DOM manipulation as it is not necessary.</p>

<code type="grid-item.directive.ts">
<span>import { Directive, HostBinding, ElementRef } from '@angular/core';</span>
<span></span>
<span>@Directive({</span>
<span>    selector: '[appGridItem]'</span>
<span>})</span>
<span>export class GridItemDirective implements OnInit {</span>
<span></span>
<span>    public top: number;</span>
<span>    public left: number;</span>
<span></span>
<span>    @HostBinding('position') get bindPosition() {</span>
<span>        return 'absolute';</span>
<span>    }</span>
<span></span>
<span>    @HostBinding('style.left.px') get bindLeft() {</span>
<span>        return this.left;</span>
<span>    }</span>
<span></span>
<span>    @HostBinding('style.top.px') get bindTop() {</span>
<span>        return this.top;</span>
<span>    }</span>
<span></span>
<span>    constructor(</span>
<span>        public element: ElementRef</span>
<span>    ) { }</span>
<span></span>
<span>}</span>
</code>

<p>The grid item initially sets the element's positioning to 'absolute', meaning that no additional CSS is required to make
    the directive work. Further to this, the grid item is purely responsible for binding values to the element's style properties
    with much of the leg work actually being done by the parent container.</p>

<h3>Usage</h3>
<p>The grid layout is easily configurable via the inputs: gridCols, gridPadding, gridFeature. These give us a succinct and declarative
    way of configuring the grid layout within our HTML making the directives reusable in varying situations. Once the directives
    are imported into the angular module they can be used within the HTML templates as follows:</p>

<code type="app.component.html">
<span>&lt;div appGridContainer [gridCols]="3" [gridPadding]="20"&gt;</span>
<span>    &lt;div appGridItem&gt;
<span>        ...</span>
<span>    &lt;/div&gt;</span>
<span>&lt;/div&gt;</span>
</code>

<h3>Conclusion</h3>
<p>CSS (Cascading Style Sheets) is a versatile and surprisingly powerful styling language. Coupled with HTML, it can easily
    be used to create some very interesting layouts. However, there are certain layouts which HTML and CSS on there own are
    not entirely equipped to create accurately and, more importantly, dynamically. This is where javascript comes into it's
    own.</p>

<p>Angular is built around the idea of web components where code is bundled up into reusable, focused and migratable chunks.
    The directive paradigm is incredibly useful here as it allows the directive to focus purely on the task of spacing elements
    in a grid pattern. It makes no assumption about what the content is, it's size, it's aesthetic appearance - it is purely
    focused on it positioning.</p>