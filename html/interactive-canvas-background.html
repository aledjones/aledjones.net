<h3>PIXEL PERFECT</h3>
<p>When designing an update to my own site,
    <a href="/">aledjones.net</a>, I decided it would be interesting to look into some form of animated background. My initial thought
    was a loopable video placed behind the content but this, though visually interesting, lacked a certain interactivity
    that I was after. It would also require relatively large files being transferred to each client viewing the site, something
    I wished to avoid. My mind wandered onto something more dynamic, generated on the client side and able to adapt to user
    input.
</p>
<figure>
    <video src="/api/asset?id=4" controls="true" alt="Masorary grid layout example"></video>
    <figcaption>Fig 1. A demonstration of the desired outcome</figcaption>
</figure>

<h3>THE IDEA</h3>
<p>After a little brainstorming, I settling on a concept consisting of falling coloured blocks. As the mouse is moved around
    the page, the closest blocks to the mouse are highlighted and expand as if to meet the cursor.</p>

<h3>PERFORMANCE CONSIDERATIONS</h3>
<p>Initial prototypes used native DOM elements to generate the desired effect. The only issue was performance. As the number
    of blocks increased the performance started to deteriorate rapidly (I was aiming for between 700 - 1000 blocks as a minimum).
    The solution was to use HTML’s canvas element.</p>
<p>HTML canvas is a native browser element that provides a surface on which to draw using Javascript. It has a collection of
    useful API’s with which to draw anything from primitive 2D shapes, lines and curves right through to full 3D primitives,
    textures and lighting (with the help of the WebGL context).</p>
<p>The DOM is a complex tree-like structure, keeping reference to every element to be rendered - it’s styling, properties, hierarchical
    data, event listeners etc. - the list goes on. Also interacting with the DOM has expensive performance implications This
    is where I started to run into performance issues due to the browser having to keep entries in the DOM as well as updating
    style bindings many thousands of times a second. Unlike the DOM, drawing on a canvas requires little overhead as the
    canvas has no concept of state as it merely renders an array of RGBA values, one set for each pixel.</p>
<p>Needless to say, even though the DOM was useful for a quick and dirty prototype, canvas was a better solution for the finished
    design.
</p>


<h3>Solution</h3>
<p>This widget was build as an Angular component. The component’s view is very minimal indeed, consisting of only a canvas element
    which is assigned the variable #canvas. This variable is used in the component’s class to retrieve this element. We work
    directly with the element to render out the finished graphics.</p>

<code type="node-ping.directive.html">
<span>&lt;canvas #canvas&gt;&lt;/canvas&gt;</span>
</code>

<p>The component’s styling is equally minimal. The canvas is sized relative to the host element so that external CSS can be
    used to style the host and adapt its appearance.</p>

<code type="node-ping.directive.scss">
<span>:host {</span>
<span>    display: block;</span>
<span>    width: 100%;</span>
<span>    height: 600px;</span>
<span>    background-color: rgb(50, 50, 50);</span>
<span>    overflow: hidden;</span>
<span>    position: relative;</span>
<span>}</span>
<span></span>
<span>canvas {</span>
<span>    position: absolute;</span>
<span>    top: 0;</span>
<span>    left: 0;</span>
<span>    height: 100%;</span>
<span>    width: 100%;</span>
<span>}</span>
</code>

<p>The following Typescript class is responsible for assigning functionality to the component.</p>

<code type="node-ping.directive.ts">
<span>import { Component, AfterViewInit, ElementRef, HostListener, ViewChild, Input, NgZone } from '@angular/core';</span>
<span></span>
<span>class Node {</span>
<span></span>
<span>    public speed = 1 + Math.random();</span>
<span></span>
<span>    constructor(public x, public y) { }</span>
<span></span>
<span>    public move(parentWidth, parentHeight) {</span>
<span>        if (this.x > parentWidth + 50) {</span>
<span>            this.x = -50;</span>
<span>        } else {</span>
<span>            this.x += .125 * this.speed;</span>
<span>        }</span>
<span>        if (this.y > parentHeight + 50) {</span>
<span>            this.y = -50;</span>
<span>        } else {</span>
<span>            this.y += .25 * this.speed;</span>
<span>        }</span>
<span>    }</span>
<span></span>
<span>    // slightly quicker than Math.abs(x)</span>
<span>    private makePositive(x) {</span>
<span>        return x &lt; 0 ? x : -x;</span>
<span>    }</span>
<span></span>
<span>    public calcProximity(Mouseleft, MouseTop) {</span>
<span>        const a = this.makePositive(Mouseleft - this.x);</span>
<span>        const b = this.makePositive(MouseTop - this.y);</span>
<span>        return Math.round(Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2)));</span>
<span>    }</span>
<span></span>
<span>    public calcOpacity(proximity) {</span>
<span>        const distance = 700;</span>
<span>        if (proximity &gt; distance) {</span>
<span>            return .1;</span>
<span>        } else {</span>
<span>            const opacity = 1.1 - (proximity / distance);</span>
<span>            return opacity &gt; 1 ? 1 : opacity;</span>
<span>        }</span>
<span>    }</span>
<span></span>
<span>    public calcScale(proximity) {</span>
<span>        const distance = 500;</span>
<span>        if (proximity &gt; distance) {</span>
<span>            return 1;</span>
<span>        } else {</span>
<span>            return 1 + ((1 - (proximity / distance)) * 10);</span>
<span>        }</span>
<span>    }</span>
<span></span>
<span>}</span>
<span></span>
<span>@Component({</span>
<span>    selector: 'app-node-ping',</span>
<span>    templateUrl: './node-ping.component.html',</span>
<span>    styleUrls: ['./node-ping.component.scss']</span>
<span>})</span>
<span>export class NodePingComponent implements AfterViewInit {</span>
<span></span>
<span>    @Input() count = 700;</span>
<span>    @Input() color = '#2196F3';</span>
<span></span>
<span>    public nodes: Array&lt;Node&gt; = [];</span>
<span>    public context: CanvasRenderingContext2D;</span>
<span>    public mouse = { x: 0, y: 0 };</span>
<span>    public bounds = { width: 0, height: 0 };</span>
<span></span>
<span>    @ViewChild('canvas') canvas: ElementRef;</span>
<span></span>
<span>    @HostListener('body:mousemove', ['$event']) moveBy(e: MouseEvent) {</span>
<span>        this.mouse = {</span>
<span>            x: e.clientX,</span>
<span>            y: e.clientY</span>
<span>        };</span>
<span>    }</span>
<span></span>
<span>    @HostListener('window:resize') resize() {</span>
<span>        this.reset();</span>
<span>    }</span>
<span></span>
<span>    constructor(</span>
<span>        public element: ElementRef,</span>
<span>        private ngZone: NgZone</span>
<span>    ) { }</span>
<span></span>
<span>    ngAfterViewInit() {</span>
<span>        this.ngZone.runOutsideAngular(() => {</span>
<span>            this.reset();</span>
<span>            this.draw();</span>
<span>        });</span>
<span>    }</span>
<span></span>
<span>    private reset() {</span>
<span></span>
<span>        this.nodes = [];</span>
<span></span>
<span>        this.bounds = this.element.nativeElement.getBoundingClientRect();</span>
<span>        this.canvas.nativeElement.height = this.bounds.height;</span>
<span>        this.canvas.nativeElement.width = this.bounds.width;</span>
<span>        this.context = this.canvas.nativeElement.getContext('2d');</span>
<span></span>
<span>        this.mouse = {</span>
<span>            x: this.bounds.width / 2,</span>
<span>            y: 0</span>
<span>        };</span>
<span></span>
<span>        for (let i = 0; i &lt; this.count; i++) {</span>
<span>            // randomly distribute along x</span>
<span>            const x = (Math.random() * (this.bounds.width + 100)) - 50;</span>
<span>            // randomly distribute along y with weighting tending towards 0</span>
<span>            const y = (Math.random() * (this.bounds.height + 100)) - 50;</span>
<span>            this.nodes.push(new Node(x, y));</span>
<span>        }</span>
<span></span>
<span>    }</span>
<span></span>
<span>    private draw() {</span>
<span></span>
<span>        this.context.clearRect(0, 0, this.bounds.width, this.bounds.height);</span>
<span>        this.context.fillStyle = this.color;</span>
<span></span>
<span>        this.nodes.forEach(node => {</span>
<span>            node.move(this.bounds.width, this.bounds.height);</span>
<span>            const proximity = node.calcProximity(this.mouse.x, this.mouse.y);</span>
<span>            const scale = node.calcScale(proximity);</span>
<span>            this.context.globalAlpha = node.calcOpacity(proximity);</span>
<span>            this.context.fillRect(node.x - (5 * scale), node.y - (5 * scale), 10 * scale, 10 * scale);</span>
<span>        });</span>
<span></span>
<span>        requestAnimationFrame(() => this.draw());</span>
<span></span>
<span>    }</span>
<span></span>
<span>}        </span>
</code>

<p>Inputs are setup to configure the block count and color. Further configuration inputs could be added to control things like
    speed and trigger proximity etc. This was not necessary for my purposes.</p>
<p>To kick things off, we hook onto the ‘AfterViewInit’ Lifecycle hook. At this point we have a guarantee that the canvas element
    is present and ready to be interacted with. The reset method is called which generates instances of the Node class for
    each of the blocks to be rendered. These nodes are assigned x and y coordinates generated randomly within the bounds
    of the canvas element. Once fully reset, the draw function is triggered setting the animation cycle going.</p>
<p>The component’s draw method manages animation by calling the ‘requestAnimaionFrame’ method. As MDN states, this is an method
    that “tells the browser that you wish to perform an animation and requests that the browser call a specified function
    to update an animation before the next repaint”.</p>
<p>The initial setup which starts the draw cycle is surrounded by Zone’s ‘runOutsideAngular’ function. Angular relies on Zone.js
    which monkey patches any asynchronous actions (http requests, events, setInterval etc.) so that change detection can
    be triggered when it is likely something has been changed that Angular should know about. This is excellent in general,
    we can worry less about making sure data bindings are updated. Zone.js will presume requestAnimationFrame will result
    in a side-effect that Angular should know about. However, because we are working with the canvas element directly and
    not through data bindings this is not the case. Running the code within ‘runOutsideAngular’ tells Zone.js not to inform
    Angular about this asynchronous action - it will not result in data changes Angular need be concerned with - a win for
    performance.
</p>
<p>The draw function is responsible for generating the image displayed on the canvas. To start, the canvas is cleared ready
    for the next frame to be rendered. Then for each node:</p>

<ol>
    <li>The position is calculated incremented from its previous position.</li>
    <li>The proximity is calculated based on the distance from the pointer the node is now positioned at.</li>
    <li>Scale and opacity are defined based on this proximity.</li>
    <li>The block is then rendered onto the canvas element.</li>
</ol>

<p>Mouse interactions are managed by ‘HostListener(“mousemove”)’. This triggers whenever the mouse is moved. As this event can
    fire extremely rapidly, it was critical that the code contained within this block was minimal. Adding any complex computations
    here would have cause catastrophic performance issues! The mouse position is stored in a variable which is used in the
    draw cycle to calculate each node’s proximity.</p>

<h3>CONCLUSION</h3>
<p>I knew that I wanted a visual element that was interesting and interactive for the background of my website. It was important
    that the aesthetic appeal did not veto the need for the web page to be performant. Through the use of modern web technologies,
    namey canvas and requestAnimationFrame, a visually interesting and performant experience was achieved.</p>
<p>Web technologies are progressing fast. Canvas opens up a wealth of possibilities for anything from simple 2d visuals like
    this through to fully fledged 3d game engines, all rendering natively within the browser. Optimisations in browsers Javascript
    engines means that performance is getting better and better expanding dramatically the scope of what browsers are capable
    of.
</p>