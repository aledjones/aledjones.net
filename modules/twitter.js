const fetch = require('node-fetch');
const FormData = require('form-data');
const consumer_key = 'fCcUasf5ZB1q9Xpa1GfLTA';
const consumer_secret = 'qVRz1B9HhNy0HsUHLmCzEx5Wcj3grfZdx85Ch7PSBI';

let lastTweet;

function getBearerToken(consumer_key, consumer_secret) {
    const encoded_key = encodeURIComponent(consumer_key);
    const encoded_secret = encodeURIComponent(consumer_secret);
    const encoded_token = Buffer.from(encoded_key + ':' + encoded_secret).toString('base64');

    return fetch('https://api.twitter.com/oauth2/token', {
        method: 'post',
        headers: {
            'Authorization': 'Basic ' + encoded_token,
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        },
        body: 'grant_type=client_credentials'
    }).then(function (resp) {
        return resp.json();
    }, function (err) {
        return err;
    });
}

function getLatestTweet(username) {
    return getBearerToken(consumer_key, consumer_secret)
        .then(function (resp) {
            return fetch('https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=' + encodeURIComponent(username) + '&count=1', {
                headers: {
                    'Authorization': 'Bearer ' + resp.access_token
                }
            });
        })
        .then(function (resp) {
            return resp.json();
        })
        .then(function (resp) {
            if (resp.errors) {
                return Promise.reject();
            } else {
                lastTweet = resp[0];
                return Promise.resolve(resp[0]);
            }
        })
        .catch(function (err) {
            if (lastTweet) {
                return Promise.resolve(lastTweet);
            } else {
                return Promise.reject('SERVER_ERROR');
            }
        })
}

// commonjs
module.exports = getLatestTweet;
// es6 default export compatibility
module.exports.default = module.exports;